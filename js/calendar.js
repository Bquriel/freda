/*************************************************************************************
Controls front end interraction betweeen calendar and server.
*************************************************************************************/

var currentView = "month";
var newEvent = { 
    title: "Busy",
    start: null,
    end: null,
    color: "red",
    url: null,
};


function onDayClick( self, date, jsEvent, view) {

        
        if( view.name == "month" ){
            currentView = "month";
            event.allDay = true;
        } else if( view.name == "agendaWeek" ){
            currentView = "agendaWeek";
            event.allDay = false;
        }
        
        newEvent.start = date.format("YYYY-MM-DD HH:mm:ss");
        date.set( "hour", date.get( "hour" ) + 2 );
        newEvent.end = date.format("YYYY-MM-DD HH:mm:ss");
        displayAddEvent();
}


// Validating Empty Field
function check_empty() {
        if ( $( "#addEventTitle" ).val() == "" ) {
            alert("Fill title and start.");
            return;
        }
            newEvent.title = $( "#addEventTitle" ).val();
            newEvent.color = $( "#addEventColor" ).val();
            newEvent.url = $( "#addEventUrl" ).val();

            $( "#calendar" ).fullCalendar( "renderEvent", newEvent, true );
            addCalendarEvent( newEvent );
            hideAddEvent();
}


//Function To Display Popup
function displayAddEvent() {
    document.getElementById( "popup" ).style.display = "block";
}


//Function to Hide Popup
function hideAddEvent(){
    document.getElementById( "popup" ).style.display = "none";
}


