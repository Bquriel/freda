/*************************************************************************************
Ajax queries to server.
*************************************************************************************/
"use strict";


//Enables admin to promote or demote people without extra loading burden.
function changeAccessLevel( id ){
	id = id.split( "button" )[ 1 ];												//id is of format buttonID.
	$.ajax({
		url: "/",
		type: "POST",
		data: {
			"ajax" : "accessLevel",
			"id": id
		},
		dataType: "json",

		success: function( changes ){											//Changes text of access level- and button field.
			$( "#button" + id ).html( changes.button );
			$( "#level" + id ).html( changes.accessLevel );
		}
	})
}


//Gets all calendar events from server and displays them.
function getCalendarEvents(){
	$.ajax({
		url: "/",
		type: "POST",
		data: {
			"ajax" : "getCalendarEvents"
		},
		dataType: "json",

		success: function( data ){

			//Each calendar event is looped through and rendered to calendar.
			data.forEach( function( element ){

				//Format: DD-MM-YYYY HH:mm:ss
				var startTime = element.start.split( " " );
				if( startTime[ 1 ] === "00:00:00" && element.end != null ){
					var endTime = element.end.split( " " );
					if( endTime[ 1 ] === "00:00:00" ){
						element.end = endTime[ 0 ];
						element.start = startTime[ 0 ];
					}
				}
				$( "#calendar" ).fullCalendar( 'renderEvent', element, true )
			})
		}
	})
}


function addCalendarEvent( event ){
	$.ajax({
		url: "/",
		type: "POST",
		data: {
			"ajax": "addCalendarEvent",
			"event": event
		},
		dataType: "html",

		success: function( data ){
		}
	})
}


function uploadImage(){
	const formData = new FormData();
	const $image = $( "#image" ).prop( "files" )[ 0 ];

	//CHECK FOR RIGHT DATATYPE

	formData.append( "image", $image);
	formData.append( "ajax", "storeImage" );

	$.ajax({
		url: "/",
		type: "POST",
		processData: false,
		contentType: false,
		data: formData /*{
			"ajax": "storeImage",
			"image": formData
		}*/,
		dataType: "text",

		success: function( data ){
			console.log( data );
			$( "#images" ).append( "<img src='" + data + "'/>" );
		}
	})
}
