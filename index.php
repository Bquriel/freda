<?php
/*************************************************************************************
Main controller of back-end. All traffic comes through here and is directed to right controllers.
*************************************************************************************/
define( "IDLEMAX", 600 );
require_once( "user.php" );
require_once( "site.phtml" );
require_once( "db.php" );
session_start();

$form       = $_POST[ "submit" ] ?? NULL;                                       //Has value if form was sent.
$action     = $_GET[ "r" ] ?? NULL;
$ajax       = $_POST[ "ajax" ] ?? NULL;                                         //Has value if ajax was used.
$user       = $_SESSION[ "user" ] ?? NULL;
$isLoggedIn = isset( $user ) ? $user->tokenAlive() : false;                     //If user is logged in, checks whether maximum idle time is full and logs user out accordingly.


if( $ajax ){                                                                    //Ajax is procecced before everything, so results can be echoed back without extra html.
    $db = new Database();
    switch( $ajax ){
        case "accessLevel":
            $result = $db->changeAccessLevel( $_POST[ "id" ] );
            echo json_encode( $result );
            break;

        case "getCalendarEvents":
            $result = $db->getCalendarEvents( 1 );                              //USER ID HERE.
            echo json_encode( $result );
            break;

        case "addCalendarEvent":
            $db->addCalendarEvent( 1, $_POST[ "event" ] );                      //$user->getID()
            break;

        case "storeImage":                                                      //TEST FOR STORING IMAGES ON SERVER. CHECK FOR RIGHT DATA TYPE.
            $image = $_FILES[ "image" ][ "tmp_name" ];
            $dest = "img/" . $_FILES[ "image" ][ "name" ];
            move_uploaded_file( $image, $dest );
            echo "$dest";
            break;

        default:
            break;
    }
    exit;
}




if( $form ){                                                                    //Directs form to form-controller. ( Login/Register )
    require( "controllers/controller_form.php" );
    exit;
}




if( $isLoggedIn ){                                                              //Users page. Directs page to approppriate controller based on url.
    displaySiteHead( "Freda - Time management made easy" );
    displaySiteNavigationLogged();

    if( $action == "logout" ){
        $user->logOut();
        header( "Location:/" );
        exit;
    } else if( $action == "admin" && $user->getAccessLevel() == User::USER_ADMIN )
        include( "controllers/controller_admin.php" );

    else if( $action == "moderator" && $user->getAccessLevel() == User::USER_MODERATOR)
        include( "controllers/controller_moderator.php" );

    else if( $action == "weather" )
        include( "controllers/controller_weather_guest.php" );

    else if( $action == "settings" )
        include( "controllers/controller_settings.php" );

    else if( $action == "cities" )
        include( "controllers/controller_weather_user.php" );

    else{
        displaySiteTitle();
        echo "<p>Nothing here</p>";
    }


} else                                                                          //Guest page.
    include( "controllers/controller_guest.php" );


displaySiteFooter();
