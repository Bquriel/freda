<?php
/*************************************************************************************
Class representing weather data of single city.
*************************************************************************************/
    class Weather{
        private $city;
        private $temperature;
        private $weatherState;

        public function __construct( $city, $temp, $state ){
            $this->city = $city;
            $this->temperature = round( $temp - 273.15, 1 );
            $this->weatherState = $state;
        }


        //Returns color by temperature.
        private function getColor(){
            if( $this->temperature < 0 ){
                $color = "#003a99";
            }else if( $this->temperature < 10 ){
                $color = "#c3c600";
            } else{
                $color = "#d34a23";
            }
            return $color;
        }

        //Displays city on screen.
        public function display(){
            $color = $this->getColor();
            echo "<div class='weatherBlock' style='background-color:$color'>";
            echo    "<p>City: $this->city</p>";
            echo    "<p>Temperature: $this->temperature</p>";
            echo    "<p>Status: $this->weatherState</p>";
            echo "</div>";
            echo "<br/><br/><br/>";
        }

        //Displays city on screen for logged user.
        public function displayLogged(){
            $color = $this->getColor();
            $name = $this->city;
            echo "<div class='weatherBlockLog' style='background-color:$color'>";
            echo    "<p>City: $this->city</p>";
            echo    "<p>Temperature: $this->temperature</p>";
            echo    "<p>Status: $this->weatherState</p>";
            echo    "<form action='/' method='get'>";
            echo    "<input type='hidden' name='r' value='cities'></input>";
            echo    "<button type='submit' name='delete' value='$name'>Delete</button>";
            echo    "</form>";
            echo "</div>";
        }

        public function getCity(){
            return $this->city;
        }
    }



?>
