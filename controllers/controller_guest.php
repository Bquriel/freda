<?php
/*************************************************************************************
Acts as guest controller for MVC.
*************************************************************************************/
require_once( "site.phtml" );

if( $action == "login" ){
    
    displaySiteHead( "Freda - Login" );
    displaySiteNavigationGuest();
    displaySiteTitle();
    displaySiteLogIn();
    
} else if( $action == "register" ){
    
    displaySiteHead( "Freda - Register" );
    displaySiteNavigationGuest();
    displaySiteTitle();
    displaySiteRegister();
    
} else if( $action == "weather" ){
    
    displaySiteHead( "Freda - Weather" );
    displaySiteNavigationGuest();
    include( "controllers/controller_weather_guest.php" );
    
} else if( $action == "calendar" ){
    
    displaySiteHead( "Freda - calendar" );
    displaySiteNavigationGuest();
    include( "controllers/controller_calendar_guest.php" );
    
} else if( $action == "image" ){
    displaySiteHead( "Image upload" );
    displaySiteNavigationGuest();
    displaySiteTitle();
    displaySiteUpload();
    
} else{
    
    displaySiteHead( "Freda - Time management made easy" );
    displaySiteNavigationGuest();
    displaySiteTitle();
    displaySiteHome();
    
}