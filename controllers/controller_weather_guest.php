<?php
/*************************************************************************************
Acts as weather controller for MVC.
*************************************************************************************/
require_once( "site.phtml" );
require_once( "weather.php" );

displaySiteTitle();
displaySiteGuestWeather();

$mem = new Memcached();
$mem->addServer( "127.0.0.1", 11211 ) or die( "Memcached not functioning" );


$city = $_GET[ "city" ] ?? null;

if( $city && $city != "" ){                                                             //If city is not memcached and input isn't empty.
    $city = str_replace(' ', '', $city);                                                //Removes white spaces from input.

    $weather = $mem->get( $city );

    if( !$weather ){
        $key = "8c10486f34cc2021727886a354762fc2";
        $data = @file_get_contents("http://api.openweathermap.org/data/2.5/find?q=$city&mode=json&APPID=$key"); //@ will prevent error from printing to user if request was bad or timed out.
        $data = json_decode( $data, true );
        if( $data["list"][0]["name"] != "" ){                                           //If api responded succesfully.
            $weather = new Weather( $data["list"][0]["name"], $data["list"][0]["main"]["temp"], $data["list"][0]["weather"][0]["main"] );
            $mem->set( $city, $weather, 60*60 );
        }
    }
    if( $weather instanceof Weather )
        $weather->display();
    else
        echo "<p class='error'>City not found</p>";
}
