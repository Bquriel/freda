<?php
/*************************************************************************************
Acts as form controller for MVC. Responsible for authentication.
*************************************************************************************/
require_once( "db.php" );

$action = $_POST[ "submit" ];

$db = new Database();

if( $action == "login" ){                                                       //Log in.

    $username = $_POST[ "username" ];
    $password = $_POST[ "password" ];
    $db->loadUser( $username, $password );                                      //Will redirect user.
}


if( $action == "register" ){                                                    //Register.
    $username = $_POST[ "username" ];
    $password = $_POST[ "password" ];
    $rePassword = $_POST[ "rePassword" ];

    if( !$db->doesUserExist( $username )){
        if( $password == $rePassword ){
            if( $db->isPasswordComplex( $password ) && strlen( $password ) > 7 ){
                $hashedPassword = password_hash( $password, CRYPT_BLOWFISH );
                $db->addUser( $username, $hashedPassword, User::USER_REGULAR );
                $_SESSION[ "status" ] = true;
                header( "Location:/?r=login" );
                exit;
            } else if( strlen( $password ) > 7 ){
                $_SESSION[ "error" ] = "Password must contain at least 1 lowercase, 1 uppercase and 1 number";
            } else{
                $_SESSION[ "error" ] = "Password must be 7 characters or longer.";
            }
        } else{
             $_SESSION[ "error" ] = "Passwords don't match.";
        }
    } else{
        $_SESSION[ "error" ] = "Username has already been taken.";
    }
    header( "Location:/?r=register" );
    exit;
}
