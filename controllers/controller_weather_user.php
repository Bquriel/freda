<?php
/*************************************************************************************
Acts as users weather panel controller for MVC.
*************************************************************************************/
require_once( "site.phtml" );
require_once( "weather.php" );
require_once( "db.php" );

displaySiteTitle();
displaySiteUserWeather();

$mem = new Memcached();
$mem->addServer( "127.0.0.1", 11211 ) or die( "Memcached not functioning" );
$db = new Database();
$cities = $db->getAllCities( $user->getID() );

$city = $_GET[ "city" ];
$city = str_replace(' ', '', $city);                                                    //Removes white spaces from input.

if( isset( $city ) && $city != "" && !in_array( $city, $cities )){                      //If city is not already set and input isn't empty.
    $db->addCity( $user->getID(), $city );
    array_push( $cities, $city );
}

$apiCities = array();                                                                   //Used for deleting cities by comparing index to $cities index, since api can return city with different name.
foreach( $cities as $key ){
    $weather = $mem->get( $key );
    
    if( !$weather instanceof Weather ){                                                                         //If city is not memcached, new one must be retrieved.
        $apiKey = "8c10486f34cc2021727886a354762fc2";
        $data = @file_get_contents("http://api.openweathermap.org/data/2.5/find?q=$key&mode=json&APPID=$apiKey"); //@ will prevent error from printing to user if request was bad or timed out.
        $data = json_decode( $data, true );
        if( $data["list"][0]["name"] != "" ){                                                                   //If api responded succesfully.
            $weather = new Weather(  $data["list"][0]["name"], $data["list"][0]["main"]["temp"], $data["list"][0]["weather"][0]["main"] );
            $mem->set( $key, $weather, 60*60 );
        }
    }
    if( $weather instanceof Weather ){
        $weather->displayLogged();
        array_push( $apiCities, $weather->getCity());                                   //Weather api might return city with different name. Array is updated so proper city may be deleted.
    } else{
        echo "<p class = 'error'>City not found</p>";
        array_push( $apiCities, "" );
    }
}


$delete = $_GET[ "delete" ];

if( isset( $delete ) && $delete != "" ){                                                 //Deleting has to be done at end because $apiCities is needed.
    $element = array_search( $delete, $apiCities );
    $delete = $cities[ $element ];
    $db->deleteCity( $delete );
    header( "Location: main.php?r=cities" );
    exit;
}