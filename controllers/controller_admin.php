<?php
/*************************************************************************************
Acts as admin controller for MVC.
*************************************************************************************/
require_once( "db.php" );
require_once( "user.php" );

displaySiteTitle();

$db = new Database();
$users = $db->getAllUsers();


echo "<div class = 'main'>";
echo "<h2>Manage user rights</h2>";
echo    "<table>";
echo        "<thead>";
echo            "<tr> <th>Username</th>";
echo            "<th>Access level</th>";
echo            "<th>Modify</th></tr>";
echo        "</thead>";

foreach( $users as $user ){
    $name = $user->getUsername();
    $id = $user->getID();
    switch( $user->getAccessLevel()){
        case User::USER_MODERATOR:
            $accessLevel = "Moderator";
            $buttonText = "Demote";
            break;
        case User::USER_REGULAR:
            $accessLevel = "Regular";
            $buttonText = "Promote";
            break;
        default:
            $accessLevel = "Unknown";
            $buttonText = "Unknown";
    }
    echo "<tr><td> $name </td><td id='level$id'> $accessLevel </td><td><button id = 'button$id' onclick='changeAccessLevel( id )'> $buttonText </button></td></tr>";

}
echo    "</table>";
echo "</div>";
