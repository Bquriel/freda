<?php
/*************************************************************************************
Defines class for logged-in user, that is used to determinate access.
*************************************************************************************/
class User{
	private $userId;
	private $userName;
	private $lastActionTime;
	private $accessLevel;

	const USER_ADMIN = 3;
	const USER_MODERATOR = 2;
	const USER_REGULAR = 1;

	public function __construct( $id, $name, $accessLevel ){
		$this->userId = $id;
		$this->userName = $name;
		$this->accessLevel = $accessLevel;
		$this->lastActionTime = time();											//Timestamp
	}

	

	public function tokenAlive(){												//Token tells if session is still legal and user hasn't been away for too long.
		$time = time();
		if( abs( $time - $this->lastActionTime ) < IDLEMAX ){
			$this->lastActionTime = $time;
			return true;
		}
		session_destroy();
		return false;
	}
	
	public function getID(){
		return $this->userId;
	}
	
	public function getAccessLevel(){
		return $this->accessLevel;
	}
	
	public function getUsername(){
		return $this->userName;
	}
	
	public function logOut(){
		session_destroy();
	}

}