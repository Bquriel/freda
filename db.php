<?php
/*************************************************************************************
Contains database related operations. Acts as model for MVC. Prepared statements are used in queries affected by user as protection against 1st order injections.
*************************************************************************************/
require_once( "user.php" );

class Database{

    private $db;


    function __construct(){
        $user       = "freda";
        $password   = "Ae328q9rtuerkg3<";

        $this->db   = new PDO( "mysql:host=localhost;dbname=freda;charset=utf8", $user, $password );
        //$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);       <- uses real prep statements.
        //$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); <- gives error handling to user.
    }

    //Adds user to database.
    function addUser( $userId, $passwordHash, $accessLevel ){
        $statement =  $this->db->prepare( "INSERT INTO `User`( `userName`, `passwordHash`, `accessLevel` ) VALUES ( ?, ?, ? ) " );
        $statement->bindParam( 1, $userId );
        $statement->bindParam( 2, $passwordHash );
        $statement->bindParam( 3, $accessLevel );                                              //Marks user as regular.
        $statement->execute();
    }

    //Adds city.
    function addCity( $userId, $city ){
        $statement = $this->db->prepare( "INSERT INTO `City`( `userID`, `name` ) VALUES ( ?, ? )" );
        $statement->bindParam( 1, $userId );
        $statement->bindParam( 2, $city );
        $statement->execute();
    }

    //Checks if the username has already been taken.
    function doesUserExist( $userName ){
        $query = $this->db->prepare( "SELECT `userName` FROM `User` WHERE `userName` = ?" );
        $query->bindParam( 1, $userName );
        $query->execute();
        $result = $query->fetchAll( PDO::FETCH_ASSOC );

        return $result == null ? false : true;
    }

    //Logs user in.
    function loadUser( $username, $password ){
        $statement = $this->db->prepare( "SELECT `id`, `userName`, `passwordHash`, `accessLevel` FROM `User` WHERE `userName` = ? LIMIT 1" );
        $statement->bindParam( 1, $username );
        $statement->execute();
        $result = $statement->fetchAll( PDO::FETCH_ASSOC );

        if( !isset( $result )){
            $_SESSION[ "error" ] = "Username or password not valid.";
            header( "Location:/?r=login" );
            exit;
        } else{
            if( password_verify( $password, $result[ 0 ][ "passwordHash" ])){

                $user = new User( $result[ 0 ][ "id" ], $result[ 0 ][ "userName" ], $result[ 0 ][ "accessLevel" ] );
                $_SESSION[ "user" ] = $user;
                header( "Location:/" );
                exit;
            } else{
                $_SESSION[ "error" ] = "Username or password not valid.";
                header( "Location:/?r=login" );
                exit;
            }
        }
    }

    //Gets all users for admin.
    function getAllUsers(){
        $statement = $this->db->query( "SELECT `id`, `userName`, `accessLevel` FROM `User` WHERE `accessLevel` != 3" );
        $results = $statement->fetchAll( PDO::FETCH_ASSOC );
        $users = array();
        foreach( $results as $result ){
            $user = new User( $result[ "id" ], $result[ "userName" ], $result[ "accessLevel" ] );
            array_push( $users, $user );
        }

        return $users;
    }

    //Returns names of cities user has saved.
    function getAllCities( $userID ){
        $statement = $this->db->query( "SELECT `name` FROM `City` WHERE `userID` = $userID" );
        $results = $statement->fetchAll( PDO::FETCH_ASSOC );
        $cities = array();
        foreach( $results as $result ){
            array_push( $cities, $result[ "name" ] );
        }

        return $cities;
    }

    //Deletes city.
    function deleteCity( $name ){
        $statement = $this->db->prepare( "DELETE FROM `City` WHERE `name` = ?" );
        $statement->bindParam( 1, $name );
        $statement->execute();
    }

    //Changes accesslevel to moderator or regular.
    function changeAccessLevel( $id ){
        $query = $this->db->prepare( "UPDATE `User` SET `accessLevel` = '2' WHERE `id` = $id" );
        $query->execute();

        $promoted = true;
        if( $query->rowCount() == 0 ){                                                              //If user is already moderator he is demoted instead. This could be achieved by querying accessLevel and changing it, but amount of queries is same either way.
            $query = $db->prepare( "UPDATE `User` SET `accessLevel` = '1' WHERE `id` = $id" );
            $query->execute();
            $promoted = false;
        }
        $newLevel   = $promoted ? "Moderator" : "Regular";
        $button     = $promoted ? "Demote" : "Promote";
        $array          = array( "accessLevel" => $newLevel, "button" => $button );
        return $array;
    }

    //Checks passwords complexity.
    function isPasswordComplex( $password ){
        return ( preg_match( "#[A-Z]+#", $password ) && preg_match( "#[a-zA-Z]+#", $password )) ? true : false;
    }

    //Retrieves calendar events for current user
    function getCalendarEvents( $userID ){
        $statement = $this->db->query( "SELECT `title`, `start`, `end`, `color`, `url` FROM `Event` WHERE `userID` = $userID" );
        $results = $statement->fetchAll( PDO::FETCH_ASSOC );
        return $results;
    }

    //Adds calendar event to database
    function addCalendarEvent( $userID, $event ){
        $statement = $this->db->prepare( "INSERT INTO `Event`( `userID`, `title`, `start`, `end`, `color`, `url` )  VALUES ( ?, ?, ?, ?, ?, ? )" );
        $statement->bindParam( 1, $userID );
        $statement->bindParam( 2, $event[ "title" ] );
        $statement->bindParam( 3, $event[ "start" ] );
        $statement->bindParam( 4, $event[ "end" ] );
        $statement->bindParam( 5, $event[ "color" ] );
        $statement->bindParam( 6, $event[ "url" ] );
        $results = $statement->execute();
    }
}
